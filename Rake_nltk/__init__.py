# -*- coding: utf-8 -*-
"""
rake-nltk module

Usage of Rake class:
    >>> from rake_nltk import Rake
    >>> r = Rake() # With language as English
    >>> r = Rake(language=<language>) # With language set to <language>
"""
from .rake import Rake
